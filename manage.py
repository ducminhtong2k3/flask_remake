from app import create_app
from app import db
from app.main.API.call_json_data import call_json
# from flask_script import Manager

app = create_app()
app.app_context().push()

# manager = Manager(app)


# @manager.command
# def init_db():
#    db.drop_all()
#    db.create_all()


# @manager.command
# def call():
#   call_json()


# @manager.command
# def run():
#    app.run(debug=True, host='0.0.0.0')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
