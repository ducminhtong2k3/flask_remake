from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_caching import Cache
from flask_cors import CORS


db = SQLAlchemy()

cache = Cache(config={'CACHE_TYPE': 'SimpleCache'})


def create_app():

    app = Flask(__name__)

    app.config.from_pyfile('config.py')

    db.init_app(app)

    cache.init_app(app)

    CORS(app)

    login_manager = LoginManager()
    login_manager.init_app(app)

    from app.main.model.User_model import User

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    from app.main.API import auth
    app.register_blueprint(auth.bp)

    from app.main.API import services
    app.register_blueprint(services.bp)
    app.add_url_rule('/', endpoint='index')

    from app.main.error_handler.error_handle import unauthenticated
    app.register_error_handler(401, unauthenticated)

    return app
