from flask import Blueprint, request, redirect, url_for, make_response
from flask_login import login_user, logout_user
from flask_cors import cross_origin
from werkzeug.security import generate_password_hash, check_password_hash

from app.main.model.User_model import User
from app import db


bp = Blueprint('auth', __name__)


@bp.route('/signup', methods=['POST', ])
def signup():
    data = request.get_json()

    user = User.query.filter_by(name=data['name']).first()

    if user:
        return redirect(url_for('index'))

    new_user = User(name=data['name'], password=generate_password_hash(data['password']))

    db.session.add(new_user)
    db.session.commit()

    return {"message": "Signup success"}


@bp.route('/login', methods=['POST', ])
def login():
    data = request.get_json()

    user = User.query.filter_by(name=data['name']).first()

    if not user or not check_password_hash(user.password, data['password']):
        return {'message': 'Login Failed'}, 401

    login_user(user)
    return {'message': 'Login Success'}


@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
