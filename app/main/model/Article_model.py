from app import db
from sqlalchemy import func


class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    context = db.Column(db.Text)
    created_at = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())

    def __repr__(self):
        return f'<Article {self.id}>'

    def json(self):
        return {"id": self.id, "title": self.title,
                "context": self.context, "created_at": self.created_at}
